/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.value;

import java.util.List;
import lk.blackout.utils.BlackOutConstants;

/**
 *
 * @author lk
 */
public class TwitterVO {
    private String tweet;
    private long tweetId;
    private double tweetRank;
    private List<TokenVO> tokenList;
    private boolean processed = false;

    /**
     * @return the tweet
     */
    public String getTweet() {
        return tweet;
    }

    /**
     * @param tweet the tweet to set
     */
    public void setTweet(String tweet) {
        this.tweet = tweet;
    }

    /**
     * @return the tweetId
     */
    public long getTweetId() {
        return tweetId;
    }

    /**
     * @param tweetId the tweetId to set
     */
    public void setTweetId(long tweetId) {
        this.tweetId = tweetId;
    }

    /**
     * @return the tweetRank
     */
    public double getTweetRank() {
        return tweetRank;
    }
    
    /**
     * @param tweetRank the tweetRank to set
     */
    public void setTweetRank(double tweetRank) {
        this.tweetRank = tweetRank;
    }    
    
    /**
     * @return the tokenList
     */
    public List<TokenVO> getTokenList() {
        return tokenList;
    }

    /**
     * @param tokenList the tokenList to set
     */
    public void setTokenList(List<TokenVO> tokenList) {
        this.tokenList = tokenList;
    }
    
    public void calculateTweetRank() {
        int unwantedWords = 0;
        int rankedWords = 0;
        double rankSum = 0.0;
        for(TokenVO aTokenVO : tokenList) {
            if(BlackOutConstants.UNWANTED_TAGS.contains(aTokenVO.getTag())) {
                unwantedWords++;
            }
            if(aTokenVO.getRank() > 0.0) {
                rankedWords++;
                rankSum += aTokenVO.getRank();
            }
        }
        
        this.tweetRank = (rankSum * rankedWords) / (tokenList.size() - unwantedWords);
    }

}
