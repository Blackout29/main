/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.value;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author lk
 */
public class TokenVO {

    private String token;
    private String tag;
    private String node;
    private String matched;
    private String matchedPart;
    private double domainScore;
    private double fbScore;
    private double pathWeight;
    private double rank = 0.0;
    private double solrMatchScore;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return the fbScore
     */
    public double getFbScore() {
        return fbScore;
    }

    /**
     * @param fbScore the fbScore to set
     */
    public void setFbScore(double fbScore) {
        this.fbScore = fbScore;
    }

    /**
     * @return the node
     */
    public String getNode() {
        return node;
    }

    /**
     * @param node the node to set
     */
    public void setNode(String node) {
        this.node = node;
    }

    /**
     * @return the pathWeight
     */
    public double getPathWeight() {
        return pathWeight;
    }

    /**
     * @param pathWeight the pathWeight to set
     */
    public void setPathWeight(double pathWeight) {
        this.pathWeight = pathWeight;
    }

    /**
     * @return the rank
     */
    public double getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(double rank) {
        this.rank = rank;
    }

    /**
     * @return the domainScore
     */
    public double getDomainScore() {
        return domainScore;
    }

    /**
     * @param domainScore the domainScore to set
     */
    public void setDomainScore(double domainScore) {
        this.domainScore = domainScore;
    }

    /**
     * @return the matched
     */
    public String getMatched() {
        return matched;
    }

    /**
     * @param matched the matched to set
     */
    public void setMatched(String matched) {
        this.matched = matched;
    }    
    

    /**
     * @return the matchedPart
     */
    public String getMatchedPart() {
        return matchedPart;
    }

    /**
     * @param matchedPart the matchedPart to set
     */
    public void setMatchedPart(String matchedPart) {
        this.matchedPart = matchedPart;
    }

    /**
     * @return the solrMatchScore
     */
    public double getSolrMatchScore() {
        return solrMatchScore;
    }

    /**
     * @param solrMatchScore the solrMatchScore to set
     */
    public void setSolrMatchScore(double solrMatchScore) {
        this.solrMatchScore = solrMatchScore;
    }
    
    public JSONObject toJson() {
        JSONObject jo = new JSONObject();
        try {

            jo.put("token", token);
            jo.put("tag", tag);
            jo.put("matched", matched);
            jo.put("matchedPart", matchedPart);
            jo.put("matchScore", solrMatchScore);
//            jo.put("Node", node);
            jo.put("domainScore", domainScore);
            jo.put("generalScore", fbScore);
            jo.put("pathWeight", pathWeight);
            jo.put("rank", rank);

        } catch (JSONException ex) {
            System.out.println("error creating token json");
        }
        return jo;
        
    }

    @Override
    public String toString() {
        String str = "";
        try {
            str = toJson().toString(2);
        } catch (JSONException ex) {
            System.out.println("error converting json to string");
        }
        return str;
    }    
    
}
