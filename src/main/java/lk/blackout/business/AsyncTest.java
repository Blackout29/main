/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.business;

/**
 *
 * @author lk
 */
public class AsyncTest implements Runnable {

    private String tId;
    
    public AsyncTest(String id) {
        this.tId = id;
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 30; i++) {
            System.out.println(i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                System.out.println("error sleep");
            }
        }
    }
    
    public String checkTwitter() {
        return "ok " + this.tId;
    }
    
    
    
}
