/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.business;

import cmu.arktweetnlp.Tagger.TaggedToken;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import lk.blackout.TweetNLP;
import lk.blackout.exceptions.BlastException;
import lk.blackout.utils.BlackOutConstants;
import lk.blackout.utils.MsgBuffer;
import lk.blackout.utils.MultiThread;
import lk.blackout.utils.solr.SolrUtils;
import lk.blackout.utils.twitter.TweetCollection;
import lk.blackout.utils.twitter.TwitterUtils;
import lk.blackout.value.TokenVO;
import lk.blackout.value.TwitterVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author lk
 */
public class ClassifyTweets implements Runnable {

    final static Logger logger = LogManager.getLogger(ClassifyTweets.class.getName());

    private final String twitterId;
    private List<TwitterVO> tweetCollection;
    TweetNLP tnlp;
    SolrUtils su;

    public ClassifyTweets(String twitterId) throws IOException {
        this.twitterId = twitterId;
        tnlp = new TweetNLP();
        su = new SolrUtils("blast-test");
    }

    public void processTweets() {
        //pre-processing
        tweetCollection = TwitterUtils.fetchTimeline(this.twitterId);

//        TwitterVO oneTwitterVO = new TwitterVO();
//        oneTwitterVO.setTweet("Find out Chelsea, Arsenal and Manchester City's #UCL draw and watch @TerryGibson88 #UCL");
//        oneTwitterVO.setTweetId(234872934);
//        tweetCollection = new ArrayList<>();
//
//        tweetCollection.add(oneTwitterVO);

        MsgBuffer.addMsg("Starting Classifier...");
        logger.trace("Starting Classifier");

        tagTweets();

        try {
            //classify tweets
            tweetCollection = disambiguateTweets(tweetCollection, true);

//            for (TwitterVO aTwitterVO : tweetCollection) {
//                System.out.println(aTwitterVO.getTweet());
//                for (TokenVO aTokenVO : aTwitterVO.getTokenList()) {
//                    System.out.println(aTokenVO.toString());
//                }
//            }

            MsgBuffer.addMsg("EOM");
        } catch (SolrServerException ex) {
            MsgBuffer.addMsg("Some problem connecting with solr");
            logger.error("Some problem connecting with solr");
        }

    }

    private void tagTweets() {
        MsgBuffer.addMsg("tagging tweets");
        tweetCollection = tnlp.posTagger(tweetCollection);
    }

    private List<TwitterVO> disambiguateTweets(List<TwitterVO> tweetVOs, boolean resolveHashtags) throws SolrServerException {
        if(resolveHashtags) {
            MsgBuffer.addMsg("disambiguating tweets");            
        }

        try {
            su.getSolrServer();
        } catch (BlastException bex) {
            MsgBuffer.addMsg("Some problem connecting with solr");
            logger.error("Unable to connect to solr.");
        }

        for (TwitterVO aTwitterVO : tweetVOs) {

            int rankedWords = 0;
            int unwantedWords = 0;
            int usefulTaggedWords = 0;
            double rankSum = 0.0;

            try {
                JSONArray ja = new JSONArray();
                for (TokenVO aTokenVO : aTwitterVO.getTokenList()) {
                    if (BlackOutConstants.UNWANTED_TAGS.contains(aTokenVO.getTag())) {
                        unwantedWords++;
                    } else {
                        if (BlackOutConstants.MY_TAGS.contains(aTokenVO.getTag())) {
                            usefulTaggedWords++;
                            aTokenVO = su.checkForNode(aTokenVO);

                            if (aTokenVO.getNode() != null) {
                                aTokenVO = calculateRank(aTokenVO);
                                rankedWords++;
                                rankSum += aTokenVO.getRank();
                            }
                        }
                        if ("#".equals(aTokenVO.getTag()) && resolveHashtags) {
                            //handle hashtags
//                            aTokenVO = resolveHashTag(aTokenVO);
                        }
                        if ("@".equals(aTokenVO.getTag())) {
                            //handle mentions
//                            aTokenVO = resolveMention(aTokenVO);
                        }
                    }

                    ja.put(aTokenVO.toJson());
                }

                double tweetRank = (rankSum * rankedWords * (aTwitterVO.getTokenList().size() - unwantedWords)) / (usefulTaggedWords * usefulTaggedWords);

                aTwitterVO.setTweetRank(tweetRank);

//                System.out.println(jo.toString(2));
                if (resolveHashtags) {
                    JSONObject jo = new JSONObject();
                    jo.put("tweet", aTwitterVO.getTweet());
                    jo.put("tweetId", aTwitterVO.getTweetId());
                    jo.put("tweetRank", tweetRank);
                    jo.put("tokens", ja);
                    MsgBuffer.addResult(jo);
                }
            } catch (JSONException ex) {
                MsgBuffer.addMsg("error creating final json");
                logger.error("error creating final json");
            }
        }

        return tweetVOs;

    }

    private TokenVO calculateRank(TokenVO aTokenVO) {

        String response;
        GenericUrl url = new GenericUrl(BlackOutConstants.SEARCH_GRAPH_URL);
        HttpTransport httpTransport = new NetHttpTransport();
        HttpRequestFactory requestFactory = httpTransport.createRequestFactory();
//        for (TokenVO aTokenVO : tokens) {
//            url.put("node", aTokenVO.getNode());
//        }
        url.put("node", aTokenVO.getNode());
        try {
            HttpRequest request = requestFactory.buildGetRequest(url);
            HttpResponse httpResponse = request.execute();
            response = httpResponse.parseAsString();

            double dist = new JSONObject(response).getJSONArray("result").getJSONObject(0).getDouble("distance");
            aTokenVO.setPathWeight(dist);
//            System.out.println(response);
            aTokenVO = freebaseLookup(aTokenVO);

            double rank = (aTokenVO.getDomainScore() / aTokenVO.getFbScore()) + (aTokenVO.getPathWeight() / 100);
            rank *= aTokenVO.getSolrMatchScore();
            aTokenVO.setRank(rank);
        } catch (IOException ex) {
//            MsgBuffer.addMsg("unable to lookup searchgraph");
            logger.error("unable to lookup searchgraph");
        } catch (JSONException ex) {
            MsgBuffer.addMsg("error reading json from searchgraph");
            logger.error("error reading json from searchgraph");
        }
        return aTokenVO;
    }

    private TokenVO freebaseLookup(TokenVO aTokenVO) {

        MultiThread mt1 = new MultiThread(aTokenVO, true);
        Thread th1 = new Thread(mt1);
        th1.start();

        MultiThread mt2 = new MultiThread(aTokenVO, false);
        Thread th2 = new Thread(mt2);
        th2.start();
        try {
            th1.join();
            th2.join();
        } catch (InterruptedException ex) {
            logger.error("some error in threads");
        }

        return aTokenVO;
    }

    private TokenVO resolveMention(TokenVO aTokenVO) throws SolrServerException {
        String description = TwitterUtils.getProfile(aTokenVO.getToken().substring(1));

        if (!"".equals(description)) {
            List<TaggedToken> taggedTokens = tnlp.arkTagger(description);
            aTokenVO.setMatched(description);
            int rankedWords = 0;
            int unwantedWords = 0;
            int usefulTaggedWords = 0;
            double rankSum = 0.0;

            for (TaggedToken aToken : taggedTokens) {
                TokenVO mentionTokenVO = new TokenVO();
                mentionTokenVO.setToken(aToken.token);
                mentionTokenVO.setTag(aToken.tag);

                if (BlackOutConstants.UNWANTED_TAGS.contains(mentionTokenVO.getTag())) {
                    unwantedWords++;
                } else {
                    if (BlackOutConstants.MY_TAGS.contains(mentionTokenVO.getTag())) {
                        usefulTaggedWords++;
                        mentionTokenVO = su.checkForNode(mentionTokenVO);

                        if (mentionTokenVO.getNode() != null) {
                            mentionTokenVO = calculateRank(mentionTokenVO);
                            rankedWords++;
                            rankSum += mentionTokenVO.getRank();
                        }
                    }
                }
            }

            double mentionRank = (rankSum * rankedWords * (taggedTokens.size() - unwantedWords)) / (usefulTaggedWords * usefulTaggedWords);

            aTokenVO.setRank(mentionRank);
        }

        return aTokenVO;
    }

    private TokenVO resolveHashTag(TokenVO aTokenVO) {
        List<TwitterVO> hashtagCollection = TwitterUtils.hashtagSearch(aTokenVO.getToken());
        hashtagCollection = tnlp.posTagger(hashtagCollection);

        double hashtagRank = 0.0;
        try {
            hashtagCollection = disambiguateTweets(hashtagCollection, false);
            for (TwitterVO aTwitterVO : hashtagCollection) {
                hashtagRank += aTwitterVO.getTweetRank();
            }
        } catch (SolrServerException ex) {
            logger.error("Some problem connecting with solr");
        }

        aTokenVO.setRank(hashtagRank / hashtagCollection.size());

        return aTokenVO;
    }

    private void cacheTweetCollection(TweetCollection tc) {
        String json = tc.toJSON();
        try {
            Files.write(Paths.get(BlackOutConstants.TWEET_CACHE_DIR + twitterId + ".json"), json.getBytes());
        } catch (IOException ex) {
            logger.error("failed to cache twitter collection for \"{}\"", twitterId);
        }
    }

    @Override
    public void run() {
        //pre-processing
        tweetCollection = TwitterUtils.fetchTimeline(this.twitterId);

        MsgBuffer.addMsg("Starting Classifier...");
        logger.trace("Starting Classifier");

        tagTweets();

        try {
            //classify tweets
            tweetCollection = disambiguateTweets(tweetCollection, true);

//            for (TwitterVO aTwitterVO : tweetCollection) {
//                System.out.println(aTwitterVO.getTweet());
//                for (TokenVO aTokenVO : aTwitterVO.getTokenList()) {
//                    System.out.println(aTokenVO.toString());
//                }
//            }
            MsgBuffer.addMsg("Finished...");
            MsgBuffer.addMsg("EOM");

        } catch (SolrServerException ex) {
            logger.error("Some problem connecting with solr");
        }
    }

    public static void main(String[] args) throws IOException {
        ClassifyTweets ct = new ClassifyTweets("SkyFootball");
//        ClassifyTweets ct = new ClassifyTweets("sardesairajdeep");;
        ct.processTweets();
    }

}
