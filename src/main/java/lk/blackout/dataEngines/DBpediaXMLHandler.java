/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.dataEngines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author lk
 */
public class DBpediaXMLHandler extends DefaultHandler {

    private List<Map<String, String>> variableBindings = new ArrayList<>();
    private Map<String, String> tempBinding = null;
    private String lastElementName = null;
    private String query = "becks";
    
    

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //System.out.println("startElement " + qName);
        if (qName.equalsIgnoreCase("result")) {
            tempBinding = new HashMap<>();
        }
        lastElementName = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        //System.out.println("endElement " + qName);
        if (qName.equalsIgnoreCase("result")) {
            if (!variableBindings.contains(tempBinding) && containsSearchTerms(tempBinding)) {
                variableBindings.add(tempBinding);
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String s = new String(ch, start, length).trim();
        //System.out.println("characters (lastElementName='" + lastElementName + "'): " + s);
        if (s.length() > 0) {
            if ("Description".equals(lastElementName)) {
                if (tempBinding.get("Description") == null) {
                    tempBinding.put("Description", s);
                }
                tempBinding.put("Description", "" + tempBinding.get("Description") + " " + s);
            }
            //if ("URI".equals(lastElementName)) tempBinding.put("URI", s);
            if ("URI".equals(lastElementName) && !s.contains("Category") && tempBinding.get("URI") == null) {
                tempBinding.put("URI", s);
            }
            if ("Label".equals(lastElementName)) {
                tempBinding.put("Label", s);
            }
        }
    }

    public List<Map<String, String>> variableBindings() {
        return variableBindings;
    }

    private boolean containsSearchTerms(Map<String, String> bindings) {
        StringBuilder sb = new StringBuilder();
        for (String value : bindings.values()) {
            sb.append(value);  // do not need white space
        }
        String text = sb.toString().toLowerCase();
        StringTokenizer st = new StringTokenizer(this.query);
        while (st.hasMoreTokens()) {
            if (!text.contains(st.nextToken().toLowerCase())) {
                return false;
            }
        }
        return true;
    }
}
