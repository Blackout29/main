/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.dataEngines;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import lk.blackout.utils.BlackOutConstants;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lk
 */
public class Freebase {

    final Logger logger = LoggerFactory.getLogger(DBPedia.class);

    public static Properties properties = new Properties();

    private String freebaseSearch(GenericUrl url) throws IOException {
        String response = null;

        properties.load(ClassLoader.getSystemResourceAsStream("freebase.properties"));
        HttpTransport httpTransport = new NetHttpTransport();
        HttpRequestFactory requestFactory = httpTransport.createRequestFactory();

        url.put("key", properties.get("API_KEY"));
        HttpRequest request = requestFactory.buildGetRequest(url);
        HttpResponse httpResponse = request.execute();
        response = httpResponse.parseAsString();

        return response;
    }

    public String freebaseSearchAPI(String token, boolean domainFlag) {
        String response = "";
        try {
            GenericUrl url = new GenericUrl(BlackOutConstants.FREEBASE_SEARCH_URL);
            url.put("query", token);
            if(domainFlag) {
                url.put("filter", "(any domain:/soccer)");
            }
            url.put("limit", "1");
            url.put("indent", "true");
            response = freebaseSearch(url);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        JSONObject jo = new JSONObject(response);
        System.out.println(jo.getJSONArray("result").getJSONObject(0).get("score"));
        
        System.out.println(response);
        return response;
    }

    public String freebaseMQLAPI(String query, String cursor) {
        String response = "";

        try {
            GenericUrl url = new GenericUrl("https://www.googleapis.com/freebase/v1/mqlread");
            url.put("query", query);
            url.put("cursor", cursor);
            response = freebaseSearch(url);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

//        System.out.println(response);
        return response;
    }

    private void cacheData(String keyword, String json) {
        try {
            Files.write(Paths.get(BlackOutConstants.FREEBASE_CACHE_DIR + keyword + ".json"), json.getBytes());
        } catch (IOException ex) {
            logger.debug("failed to cache result for \"{}\"", keyword);
        }
    }

    private void MQLQuerygenerator() {
        boolean more = true;
        String cursor = "";
        String response = "";
        int count = 0;
        int num = 1;
        String query = "[{\"limit\":200,\"id\":null,\"name\":null,\"mid\":null,\"type\":\"/soccer/football_player\",\"/common/topic/alias\":[],\"/soccer/football_player/position_s\":[],\"/people/person/nationality\":[]}]";
        while (!cursor.equalsIgnoreCase("false")) {
            response = freebaseMQLAPI(query, cursor);

            JSONObject jo = new JSONObject(response);
            JSONArray ja = jo.getJSONArray("result");
            count += ja.length();
            cacheData("football-players/" + num, response);
            num++;
            System.out.println("new count: " + count);

            try {
                cursor = jo.getString("cursor");
            } catch (Exception ex) {
                System.out.println(jo.get("cursor"));
                break;
            }
        }
        
        System.out.println("num files: " + num);
    }

    public void parseResult(String filename) {
        String json = "";
        File f = new File(BlackOutConstants.FREEBASE_CACHE_DIR + filename + ".json");
        try {
            json = FileUtils.readFileToString(f);
        } catch (IOException ex) {
            logger.debug("failed to read file - \"{}\"", filename);
        }

        JSONArray resultsArr = new JSONObject(json).getJSONArray("result");

        for (int i = 0; i < resultsArr.length(); i++) {

            JSONObject res = resultsArr.getJSONObject(i);
            String mid = res.getString("mid");
            String id = res.getString("id");
            String name = res.getString("name");

            System.out.println("mid: " + mid);
            System.out.println("id: " + id);
            System.out.println("name: " + name);

            JSONArray aliases = res.getJSONArray("/common/topic/alias");
            for (int j = 0; j < aliases.length(); j++) {
                System.out.println(aliases.get(j));
            }
            System.out.println("==================================================");
        }

        System.out.println(resultsArr.length());
    }

    public static void main(String[] args) {
        Freebase instance = new Freebase();
        String token = "bayern";
        instance.freebaseSearchAPI(token, true);
        instance.freebaseSearchAPI(token, false);
//        instance.MQLQuerygenerator();
    }
}
