/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout;

import lk.blackout.utils.BlackOutConstants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import lk.blackout.utils.twitter.TweetCollection;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 *
 * @author lk
 */
public class App {

    final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws URISyntaxException, IOException {
        App instance = new App();
//        instance.testxyz();
//        instance.testApp2();
//        instance.serializeobj();
        instance.deserializeobj();

    }

    public void testxyz() throws IOException {
        URL url = ClassLoader.getSystemResource("noresult");
        List<String> noresultList = Files.readAllLines(Paths.get(url.getPath()), Charset.defaultCharset());
        System.out.println(noresultList);
    }

    public TweetCollection buildTweetcol(String twitterid) {
        TweetCollection tc = new TweetCollection();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(BlackOutConstants.TWEET_CACHE_DIR + twitterid + ".txt"), "UTF8"));
        } catch (UnsupportedEncodingException | FileNotFoundException ex) {
            ex.printStackTrace();
        }

        String tweet;
        try {
            while ((tweet = br.readLine()) != null) {
//                System.out.println(tweet);
//                System.out.println("------------------------------------------");
                tc.addTweet(tweet);
            }
//            System.out.println(tc.toJSON());
        } catch (IOException | JSONException ex) {
            ex.printStackTrace();
        }
        return tc;
    }

    public void fetchTwitterTimeline(String twitterid) throws FileNotFoundException, IOException {

        File fout = new File(BlackOutConstants.TWEET_CACHE_DIR + twitterid + ".txt");
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        List<Status> statuses = null;
        int pagenum = 1;
        boolean more = true;
        try {
            Twitter twitter = TwitterFactory.getSingleton();
            while (more) {
                statuses = twitter.getUserTimeline(twitterid, new Paging(pagenum).count(100));
                for (Status status : statuses) {
//                    System.out.println(status.getUser().getName() + ":"
//                            + status.getText());
                    bw.write(status.getText());
                    bw.newLine();
                }
//                System.out.println("============================================================");
                if (statuses.size() == 100) {
                    pagenum++;
                } else {
                    more = false;
                }
            }
            bw.close();
        } catch (TwitterException ex) {
            logger.debug("twitter exception");
        }
    }

    public List<String[]> tokenizeTweets(List<Status> statuses) {
        InputStream modelIn = this.getClass().getClassLoader().getResourceAsStream("en-token.bin");
        List<String[]> tokStatus = new ArrayList<>();
        try {
            TokenizerModel model = new TokenizerModel(modelIn);
            Tokenizer tokenizer = new TokenizerME(model);

            for (Status status : statuses) {
                String tokens[] = tokenizer.tokenize(status.getText());
                tokStatus.add(tokens);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (modelIn != null) {
                try {
                    modelIn.close();
                } catch (IOException e) {
                }
            }
        }

        return tokStatus;
    }

    public void tagTweets(List<String[]> tokStatus) {
        InputStream posmodelstream = null;
        List<String[]> tags = new ArrayList<>();

        try {

            posmodelstream = this.getClass().getClassLoader().getResourceAsStream("en-pos-maxent.bin");
            POSModel model = new POSModel(posmodelstream);

            POSTaggerME tagger = new POSTaggerME(model);

            for (String[] tokens : tokStatus) {
                String tmptags[] = tagger.tag(tokens);
//                for()
                tags.add(tmptags);
            }

        } catch (IOException e) {
            // Model loading failed, handle the error
            e.printStackTrace();
        } finally {
            if (posmodelstream != null) {
                try {
                    posmodelstream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public void testjson() throws IOException {
        File f = new File("/Users/lk/Desktop/becks.json");
        String cont = FileUtils.readFileToString(f);
        JSONArray arr = new JSONObject(cont).getJSONArray("results");
        System.out.println("num results : " + arr.length());

        for (int i = 0; i < arr.length(); i++) {
            JSONObject res = arr.getJSONObject(i);
            String label = res.getString("label");
            System.out.println("result " + i + " " + label);

            JSONArray classes = res.getJSONArray("classes");
            System.out.println("CLASSES");
            for (int j = 0; j < classes.length(); j++) {
                String cls = classes.getJSONObject(j).getString("label");
                System.out.println(cls);
            }

            JSONArray cats = res.getJSONArray("categories");
            System.out.println("CATEGORIES");
            for (int j = 0; j < cats.length(); j++) {
                String cat = cats.getJSONObject(j).getString("label");
                System.out.println(cat);
            }
        }

    }

//    public void testApp() throws IOException {
////        String twitterid = "chelseafc";
//        String twitterid = "test";
////        fetchTwitterTimeline(twitterid);
//        TweetCollection tc = buildTweetcol(twitterid);
//
//        NavigableMap<Integer, String> tweets = tc.getTweets();
//        TweetNLP tnlp = new TweetNLP();
//
//        for (Entry<Integer, String> tweet : tweets.entrySet()) {
//            List<Tagger.TaggedToken> taggedTokens = tnlp.arkPOSTagger(tweet.getValue());
//            logger.info(tweet.getValue());
//            for (Tagger.TaggedToken aToken : taggedTokens) {
////                System.out.printf("%s\t%s\n", token.tag, token.token);
//                if ("N".equals(aToken.tag) || "^".equals(aToken.tag)) {
//                    TokenEnhancer encdbToken = new DBpediaEnhancer();
//
//                    encdbToken.enhance((aToken.token).toLowerCase());
//
//                    UndirectedGraph<String, DefaultEdge> tokenGraph = encdbToken.getGraph();
//                    Set<DefaultEdge> edges = tokenGraph.edgesOf((aToken.token).toLowerCase());
//
//                    UndirectedSubgraph<String, DefaultEdge> sg = new UndirectedSubgraph<>(tokenGraph, null, edges);
//                    System.out.println(sg.toString());
//
////                    UndirectedSubgraph<String, DefaultEdge> subgraph = 
////                    tokenGraph.
////                    DBPedia dbpedia = new DBPedia();
////                    if(dbpedia.lookup((token.token).toLowerCase())) {
////                        
////                    }
//                }
//
//                if ("#".equals(aToken.tag)) {
//                    //handle hashtags
//                }
//                if ("@".equals(aToken.tag)) {
//                    //handle mentions
//                }
//            }
//            System.out.println("============================================================");
//        }
//
//    }
//
//    public void testApp2() throws IOException {
//        String twitterid = "test";
//        TweetCollection tc = buildTweetcol(twitterid);
//
//        int numTokens = 0;
//        float sum = 0.0f;
//
//        NavigableMap<Integer, String> tweets = tc.getTweets();
//        TweetNLP tnlp = new TweetNLP();
//        for (Entry<Integer, String> tweet : tweets.entrySet()) {
//            List<Tagger.TaggedToken> taggedTokens = tnlp.arkPOSTagger(tweet.getValue());
//            logger.info(tweet.getValue());
//            for (Tagger.TaggedToken aToken : taggedTokens) {
////                System.out.printf("%s\t%s\n", token.tag, token.token);
//                if ("N".equals(aToken.tag) || "^".equals(aToken.tag)) {
//
//                    numTokens++;
//                    sum += calculateWeight(aToken.token);
//                }
//
//                if ("#".equals(aToken.tag)) {
//                    //handle hashtags
//                }
//                if ("@".equals(aToken.tag)) {
//                    //handle mentions
//                }
//            }
//            System.out.println("============================================================" + sum);
//        }
//    }

    public float calculateWeight(String token) {
//        System.out.println("fetching tken: " + token);
        SearchGraph sgraph = new SearchGraph();
        int distance = sgraph.searchKeyword(token.toLowerCase());
        System.out.println(distance);
        if (distance == 0) {
            return 0;
        }
        return 100 / distance;
    }

    public void serializeobj() {
        String filename = "/Users/lk/Desktop/myobj";
        SearchGraph sgraph = new SearchGraph();
        sgraph.wightedGraphTest();
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = new FileOutputStream(filename);
            out = new ObjectOutputStream(fos);
            out.writeObject(sgraph);
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void deserializeobj() {
        String filename = "/Users/lk/Desktop/myobj";
        FileInputStream fis = null;
        ObjectInputStream in = null;
        SearchGraph sgraph = new SearchGraph();
        
        try {
            fis = new FileInputStream(filename);
            in = new ObjectInputStream(fis);
            sgraph = (SearchGraph) in.readObject();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println(sgraph.wgraph.toString());
    }

}
