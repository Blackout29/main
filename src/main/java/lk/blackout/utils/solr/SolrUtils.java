/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.utils.solr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lk.blackout.exceptions.BlastException;
import lk.blackout.utils.BlackOutConstants;
import lk.blackout.value.TokenVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

/**
 *
 * @author lk
 */
public class SolrUtils {

    final static Logger logger = LogManager.getLogger(SolrUtils.class.getName());

    private static SolrServer solrServer = null;
    private static String core;

    public SolrUtils() {
    }

    public SolrUtils(String core) {
        this.core = core;
    }

    public String getCore() {
        return this.core;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public SolrServer getSolrServer() throws BlastException {
        if (core == null || "".equals(core)) {
            throw new BlastException("please set core using setCore() method");
        }

        if (solrServer == null) {
            solrServer = new HttpSolrServer(BlackOutConstants.SOLR_URL + this.core);
        }
        return solrServer;
    }

    public void addNodeToSolr(String node, String aka) throws SolrServerException, IOException, BlastException {
        SolrServer solrServer = getSolrServer();
        SolrQuery query = new SolrQuery();
        query.setQuery("node:" + node);
//        System.out.println(query);
        SolrDocumentList sdl = solrServer.query(query).getResults();
        SolrDocument sd;
        SolrInputDocument doc;
        if (sdl.isEmpty()) {
            sd = new SolrDocument();
            doc = ClientUtils.toSolrInputDocument(sd);
            doc.setField("node", node);
        } else {
            sd = sdl.get(0);
            doc = ClientUtils.toSolrInputDocument(sd);
        }

        doc.addField("aka", aka);
        doc.addField("aka", "aka1");
        doc.addField("aka", "aka2");
        doc.addField("id", "testid");
        doc.addField("mid", "testmid");
        doc.addField("type", "main");

        solrServer.add(doc);
        solrServer.commit();
    }

    public QueryResponse querySolr(SolrQuery query) throws SolrServerException {
//        SolrDocumentList sdl = new SolrDocumentList();
        logger.debug(query.toString());
        QueryResponse rsp = solrServer.query(query);

        return rsp;

    }

    public TokenVO checkForNode(TokenVO aTokenVO) throws SolrServerException {
//        String node = null;
        String token = aTokenVO.getToken();
        double matchScore = 0.0;
        
        
        if(token.startsWith("#") || token.startsWith("@")) {
            token = token.substring(1);
        }

        String qry1 = "aka_orig:\"" + token + "\"^100 OR aka1:\"" + token + "\"^50";
        String qry2 = "aka2:\"" + token + "\"";
        String qry3 = "aka2:*" + token + "*";

        SolrQuery query = new SolrQuery();
        query.addField("mid");
        query.addField("name");
        query.addField("type");
        query.addSort("score", SolrQuery.ORDER.desc);
        query.setHighlight(true);
        query.setHighlightRequireFieldMatch(true);
        query.setParam("hl.fl", "aka_orig, aka1, aka2");
        query.setRows(1);

        QueryResponse rsp;
        SolrDocumentList sdl = null;
        SolrDocument sd = null;

        query.setQuery(qry1);
//        rsp = querySolr(query);
        rsp = solrServer.query(query);
        sdl = rsp.getResults();
        if (sdl.isEmpty()) {
            query.setQuery(qry2);
//            rsp = querySolr(query);
            rsp = solrServer.query(query);
            sdl = rsp.getResults();
            if (sdl.isEmpty()) {
                query.setQuery(qry3);
//                rsp = querySolr(query);
                rsp = solrServer.query(query);
                sdl = rsp.getResults();
                if (!sdl.isEmpty()) {
                    matchScore = 0.3;
                }
            } else {
                matchScore = 0.8;
            }
        } else {
            matchScore = 1.0;
        }

        if (!sdl.isEmpty()) {
            sd = sdl.get(0);
            sd.setField("matchScore", matchScore);
            List<String> highlights = null;

            highlights = rsp.getHighlighting().get((String) sd.getFieldValue("mid")).get("aka_orig");
            if (highlights == null) {
                highlights = rsp.getHighlighting().get((String) sd.getFieldValue("mid")).get("aka2");
                if (highlights == null) {
                    highlights = rsp.getHighlighting().get((String) sd.getFieldValue("mid")).get("aka1");
                }
            }

            aTokenVO.setNode((String) sd.getFieldValue("mid"));
            aTokenVO.setMatched((String) sd.getFieldValue("type") + " -> " + (String) sd.getFieldValue("name"));
            aTokenVO.setSolrMatchScore(matchScore);
            aTokenVO.setMatchedPart(highlights.get(0));

//            node = (String) sd.getFieldValue("mid");
        }
        return aTokenVO;
    }

    public void xxremoveAllData() {
        try {
            SolrServer solrserver = getSolrServer();
            solrServer.deleteByQuery("*:*");
        } catch (BlastException | IOException | SolrServerException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) throws SolrServerException, IOException, BlastException {
        SolrUtils instance = new SolrUtils("blast-test");
        instance.getSolrServer();
//        instance.checkForNode("Messi");
//        System.out.println(instance.checkForNode("pragati"));
//        System.out.println(instance.checkForNode("asd"));
    }
}
