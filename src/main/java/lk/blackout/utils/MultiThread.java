/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.utils;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lk.blackout.value.TokenVO;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author lk
 */
public class MultiThread implements Runnable{

    private static final String API_KEY = "AIzaSyC5GZAHLT4DXlh5tjpLZX4WBgkIwd1-ncI";
//    private static final Properties properties = new Properties();
    private String response;
    private final boolean domainFlag;
    private TokenVO aTokenVO;
    
    public MultiThread(TokenVO aTokenVO, boolean domainFlag) {
        this.aTokenVO = aTokenVO;
        this.domainFlag = domainFlag;
    }

    private void freebaseSearch(GenericUrl url) throws IOException {

//        properties.load(ClassLoader.getSystemResourceAsStream("freebase.properties"));
        HttpTransport httpTransport = new NetHttpTransport();
        HttpRequestFactory requestFactory = httpTransport.createRequestFactory();

//        url.put("key", properties.get("API_KEY"));
        url.put("key", API_KEY);
        HttpRequest request = requestFactory.buildGetRequest(url);
        HttpResponse httpResponse = request.execute();
        response = httpResponse.parseAsString();
        
        JSONObject jo;
        double score = 0.0;
        try {
            jo = new JSONObject(response);
            score = (double) jo.getJSONArray("result").getJSONObject(0).get("score");
        } catch (JSONException ex) {
            System.out.println(response);
            System.out.println(aTokenVO.getToken());
            System.out.println("problem reading json result");
        }
        
        if(domainFlag)
            aTokenVO.setDomainScore(score);
        else
            aTokenVO.setFbScore(score);
        
    }     
    
    @Override
    public void run() {        
        try {
            GenericUrl url = new GenericUrl(BlackOutConstants.FREEBASE_SEARCH_URL);
            url.put("query", aTokenVO.getToken());
            if(domainFlag) {
                url.put("filter", "(any domain:/soccer)");
            }
            url.put("limit", "1");
//            url.put("indent", "true");
            freebaseSearch(url);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        System.out.println(response);
    }
    
    public String freebaseSearchAPI(String token, boolean domainFlag) {
        String response = "";
        try {
            GenericUrl url = new GenericUrl(BlackOutConstants.FREEBASE_SEARCH_URL);
            url.put("query", token);
            if(domainFlag) {
                url.put("filter", "(any domain:/soccer)");
            }
            url.put("limit", "1");
            url.put("indent", "true");
            freebaseSearch(url);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        System.out.println(response);
        return response;
    }    
    
    
    
}
