/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.utils.twitter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import lk.blackout.utils.BlackOutConstants;
import lk.blackout.utils.MsgBuffer;
import lk.blackout.value.TwitterVO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 *
 * @author lk
 */
public class TwitterUtils {

    final static Logger logger = LogManager.getLogger(TwitterUtils.class.getName());

    public TwitterUtils() {

    }

    /**
     * Fetch twitter timeline for provided twitterid and write it to the file of
     * same name
     *
     * @param twitterId
     * @return string of tweets
     */
    public static List<TwitterVO> fetchTimeline(String twitterId) {

//        StringBuilder sb = new StringBuilder();
        int maxTweets = 9999;
        int tweetsPerPage = 100;
        List<TwitterVO> tweets = new ArrayList<>();
        int pagenum = 1;
        boolean more = true;
        try {
            MsgBuffer.addMsg("fetching tweets...");
            Twitter twitter = TwitterFactory.getSingleton();
            List<Status> statuses = null;
            while (more) {
                statuses = twitter.getUserTimeline(twitterId, new Paging(pagenum).count(tweetsPerPage));
                for (Status status : statuses) {
                    TwitterVO aTwitterVO = new TwitterVO();
                    aTwitterVO.setTweet(status.getText());
                    aTwitterVO.setTweetId(status.getId());
                    tweets.add(aTwitterVO);
//                    System.out.println(status.getText());
//                    System.out.println(status.getId());
                }
//                System.out.println("============================================================");
                if (statuses.size() == tweetsPerPage && (tweets.size() + tweetsPerPage + 1 <= maxTweets)) {
                    pagenum++;
                    more = true;
                } else {
                    more = false;
                }
            }
            logger.info("total tweets fetched from {}: {}", twitterId, tweets.size());
            MsgBuffer.addMsg("total tweets fetched from @" + twitterId + ": " + tweets.size());
        } catch (TwitterException ex) {
            logger.error("error fetching tweets for twitterid: {}. With message {}", twitterId, ex.getErrorMessage());
            MsgBuffer.addMsg("error fetching tweets for" + twitterId + ": " + tweets.size());
        }

        return tweets;
    }

    /**
     * Fetches user profile given twitterid
     *
     * @param twitterid
     * @return twitter profile description
     */
    public static String getProfile(String twitterid) {

        String description = "";
        Twitter twitter = TwitterFactory.getSingleton();
        try {
            User user = twitter.showUser(twitterid);
            description = user.getDescription();
            System.out.println(user);
            System.out.println(description);
            System.out.println(user.getLocation());
            System.out.println(user.getTimeZone());
            System.out.println(user.getUtcOffset());
        } catch (TwitterException ex) {
            logger.debug("failed to fetch profile for {} with message {}", twitterid, ex.getErrorMessage());
        }

        return description;
    }

    /**
     * fetch 10 most recent tweets for a particular hashtag
     *
     * @param hashtag
     * @return List<TwitterVO>
     */
    public static List<TwitterVO> hashtagSearch(String hashtag) {
        List<TwitterVO> tweets = new ArrayList<>();
        Query query = new Query(hashtag);
        query.count(10);
        QueryResult qr;
        try {
            Twitter twitter = TwitterFactory.getSingleton();
            qr = twitter.search(query);
            List<Status> hashTweets = qr.getTweets();
            for (Status tweet : hashTweets) {
                TwitterVO aTwitterVO = new TwitterVO();
                aTwitterVO.setTweet(tweet.getText());
                aTwitterVO.setTweetId(tweet.getId());
                tweets.add(aTwitterVO);
                
            }
        } catch (TwitterException ex) {
            logger.debug("twitter exception");
        }

        return tweets;
    }
    
    
    public void cacheTweets(String twitterId) {

        StringBuilder sb = new StringBuilder();
        
        List<Status> statuses = null;
        int pagenum = 1;
        boolean more = true;
        try {
            Twitter twitter = TwitterFactory.getSingleton();
            while (more) {
                statuses = twitter.getUserTimeline(twitterId, new Paging(pagenum).count(100));
                for (Status status : statuses) {
//                    System.out.println(status.getUser().getName() + ":"
//                            + status.getText());
                    
                    sb.append(status.getText());
                    sb.append(System.lineSeparator());
                }
//                System.out.println("============================================================");
                if (statuses.size() == 100) {
                    pagenum++;
                } else {
                    more = false;
                }
            }
            logger.info("total tweets fetched from {}: ~{}", twitterId, (pagenum * 100));
        } catch (TwitterException ex) {
            logger.error("error fetching tweets for twitterid: " + twitterId);
        }
        
        try {
            //        File f = new File(BlackOutConstants.DBPEDIA_CACHE_DIR + keyword + ".json");
            Files.write(Paths.get("/Users/lk/Desktop/tweets/" + twitterId + ".txt"), sb.toString().getBytes());
        } catch (IOException ex) {
            logger.debug("failed to cache tweets for \"{}\"", twitterId);
        }
        
    }    

    public static void main(String[] args) {
        TwitterUtils instance = new TwitterUtils();

        instance.cacheTweets("SkyFootball");
//        instance.getProfile("SkyFootball");
//        instance.hashtagSearch("#UCL");

    }

}
