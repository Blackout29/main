/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lk.blackout.utils;

import java.util.HashSet;

/**
 *
 * @author lk
 */
public class BlackOutConstants {
    
    public static final String TWEET_CACHE_DIR = "/Users/lk/Thesis/datacache/tweets/";
    public static final String DBPEDIA_CACHE_DIR = "/Users/lk/Thesis/datacache/dbpedia/";
    public static final String FREEBASE_CACHE_DIR = "/Users/lk/Thesis/datacache/freebase/";
    public static final String NO_RESULT_FILE = "noresult";
    public static final String SOLR_URL = "http://localhost:8983/solr/";
    public static final String SEARCH_GRAPH_URL = "http://localhost:8080/SearchGraph/api/graph";
    public static final String FREEBASE_SEARCH_URL = "https://www.googleapis.com/freebase/v1/search";
    
    
    //maybe there is a better way to do this?
    public static final HashSet<String> MY_TAGS;
    public static final HashSet<String> UNWANTED_TAGS;
    static 
    {
        MY_TAGS = new HashSet<>();
        MY_TAGS.add("^");
        MY_TAGS.add("N");
        MY_TAGS.add("Z");
        MY_TAGS.add("M");
        
        UNWANTED_TAGS = new HashSet<>();
        UNWANTED_TAGS.add("E");
        UNWANTED_TAGS.add("&");
        UNWANTED_TAGS.add("D");
        UNWANTED_TAGS.add("P");
        UNWANTED_TAGS.add(",");
        UNWANTED_TAGS.add("!");
        UNWANTED_TAGS.add("$");
        UNWANTED_TAGS.add("U");
        UNWANTED_TAGS.add("tilde");
    }
    
    
    public static String SOLR_SEARCH_FIELD_1 = "aka_orig";
    public static String SOLR_SEARCH_FIELD_2 = "aka1";
    public static String SOLR_SEARCH_FIELD_3 = "aka2";
}
