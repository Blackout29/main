/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author lk
 */
public class MsgBuffer {

    private static final ArrayList<String> msgBuffer = new ArrayList<>();
    private static final ArrayList<Object> resultBuffer = new ArrayList<>();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy:HH:mm:SS");

    public static void addMsg(String msg) {
        if("EOM".equals(msg)) {
            msgBuffer.add(msg);
        } else {
            Date date = new Date();
            msgBuffer.add(dateFormat.format(date) + " " + msg);
        }
    }
    
    public static void addResult(Object result) {
        resultBuffer.add(result);
    }

    public static JSONObject getMsgs() {
        JSONObject jo = new JSONObject();
        JSONArray jArrayMsgs = new JSONArray();
        JSONArray jArrayResults = new JSONArray();
        for(String msg : msgBuffer) {
            jArrayMsgs.put(msg);
        }
        msgBuffer.clear();
        
        for(Object result : resultBuffer) {
            jArrayResults.put(result);
        }
        resultBuffer.clear();
        try {
            jo.put("msgs", jArrayMsgs);
            jo.put("results", jArrayResults);
        } catch (JSONException ex) {
            System.out.println("error creating msgbuffer json");
        }
        return jo;
    }
    
    public static void clearMsgs() {
        msgBuffer.clear();
        resultBuffer.clear();
    }

    public static void main(String[] args) {
        MsgBuffer.addMsg("asd");
    }
}
