package lk.blackout;

import lk.blackout.dataEngines.DBpediaXMLHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.xml.sax.SAXException;

public class WikiLookup {

    String keyword;
    
    public WikiLookup() throws IOException {

    }
    
    public static void main(String[] args) throws IOException {
       WikiLookup instance = new WikiLookup();
//       InputStream is = instance.dbPediaLookup();
       instance.parseResult();
    }

    public InputStream dbPediaLookup(String keyword) throws IOException {
         this.keyword = keyword;
        
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpget = new HttpGet("http://lookup.dbpedia.org/api/search.asmx/KeywordSearch?QueryString=becks");
//        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = httpclient.execute(httpget);
        InputStream is = response.getEntity().getContent();
        
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(is));

        StringBuilder result = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        System.out.println(result.toString());        
        
        return is;
    }
    
    
    public void parseResult() throws IOException {

            SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser sax = factory.newSAXParser();
            DBpediaXMLHandler dbpediaHandler = new DBpediaXMLHandler();
            sax.parse(new File("/Users/lk/Desktop/becks.xml"), dbpediaHandler);
        } catch (ParserConfigurationException | SAXException ex) {
           ex.printStackTrace();
        }
    }
}
