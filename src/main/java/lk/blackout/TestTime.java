/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout;

import java.io.IOException;
import lk.blackout.business.ClassifyTweets;

/**
 *
 * @author lk
 */
public class TestTime {
    
    
    public static void main(String[] args) throws IOException {
        final long startTime = System.currentTimeMillis();
        System.out.println("Start time: " + startTime);        
        
        new ClassifyTweets("SkyFootball").processTweets();
        final long endTime = System.currentTimeMillis();
        
        System.out.println("End time: " + endTime);        
        
        System.out.println("Total execution time: " + (endTime - startTime));
        
        
    }
    
}
