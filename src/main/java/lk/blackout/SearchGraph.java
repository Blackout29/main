/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.SimpleWeightedGraph;

/**
 *
 * @author lk
 */
public class SearchGraph implements Serializable{

//    SimpleWeightedGraph<String, DefaultWeightedEdge> searchGraph;
    UndirectedGraph<String, DefaultEdge> searchGraph;
    SimpleWeightedGraph<String, DefaultWeightedEdge> wgraph;
    
    String root = "football";
    HashMap<String, String> graphMap;
//    HashSet<String> level3;

    public SearchGraph() {
//        searchGraph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
        searchGraph = new SimpleGraph<>(DefaultEdge.class);
        searchGraph.addVertex(root);
        graphMap = new HashMap<>();
        String[] level1 = {"soccer", "bayern munich", "manchester united", "barcelona"};
        String[] level2 = {"clubs", "germany,neuer", "england,beckham", "spain,messi"};
        for (int i = 0; i < level1.length; i++) {
            graphMap.put(level1[i], level2[i]);
        }
        this.generateGraph();

    }

    public void generateGraph() {

        for (Map.Entry<String, String> entry : graphMap.entrySet()) {
            String[] tmp = entry.getValue().split(",");
            if (searchGraph.addVertex(entry.getKey())) {
                searchGraph.addEdge(root, entry.getKey());
            }
            if (searchGraph.addVertex(tmp[0])) {
                searchGraph.addEdge(entry.getKey(), tmp[0]);
            }
            if (tmp.length > 1) {
                if (searchGraph.addVertex(tmp[1])) {
                    searchGraph.addEdge(entry.getKey(), tmp[1]);
                }

            }
        }
        if (searchGraph.addVertex("sureka")) {
            searchGraph.addEdge("clubs", "sureka");
        }

    }

    public int searchKeyword(String target) {
        System.out.println("target: " + target);

        List edges = null;
        try {
            edges = DijkstraShortestPath.findPathBetween(searchGraph, root, target);
        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
            return 0;
        }

        return edges.size() + 1;
    }

    public void wightedGraphTest() {

        wgraph = new SimpleWeightedGraph<>(DefaultWeightedEdge.class);
        DefaultWeightedEdge edge;
        wgraph.addVertex(root);
        wgraph.addVertex("node1");
        edge = wgraph.addEdge(root, "node1");
        wgraph.setEdgeWeight(edge, 6);
        
        wgraph.addVertex("node2");
        edge = wgraph.addEdge(root, "node2");
        wgraph.setEdgeWeight(edge, 4);
        
        wgraph.addVertex("node3");
        edge = wgraph.addEdge("node2", "node3");
        wgraph.setEdgeWeight(edge, 2);
        
        wgraph.addVertex("node4");
        edge = wgraph.addEdge("node4", "node3");
        wgraph.setEdgeWeight(edge, 5);
        
    }
    
    public void tmpfunc() {
        DijkstraShortestPath dsp = new DijkstraShortestPath(wgraph, root, "node3");
        
        System.out.println(dsp.getPathLength());
        
        List<DefaultWeightedEdge> edges = DijkstraShortestPath.findPathBetween(wgraph, root, "node3");
        
        for(DefaultWeightedEdge edg : edges) {
            System.out.println(edg.toString());
        }
        
//        System.out.println(edges.get(0).getClass());
        System.out.println(edges.size());        
    }

    public static void main(String[] args) {
        SearchGraph instance = new SearchGraph();
        instance.wightedGraphTest();
        instance.tmpfunc();
        
        
        
//        instance.generateGraph();

//        instance.searchKeyword("football");        
//        System.out.println(instance.searchKeyword("sureka"));
//        System.out.println(instance.searchGraph.toString());
//        StringWriter sr = new StringWriter();
//        VertexNameProvider vidp = new IntegerNameProvider<>();
//        VertexNameProvider vlp = new StringNameProvider<String>();
//        EdgeNameProvider elp = new StringEdgeNameProvider<String>();
//        DOTExporter dex = new DOTExporter(vidp, vlp, elp);
//        dex.export(sr, instance.searchGraph);
//        System.out.println(sr.toString());
    }

}
