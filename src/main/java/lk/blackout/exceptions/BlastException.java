/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.blackout.exceptions;

/**
 *
 * @author lk
 */
public class BlastException extends Exception {
    
    public BlastException() {
        super();
    }
    
    public BlastException(String message) {
        super(message);
    }
}
