/* 
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */

'use strict';

angular.module('main.directives', [])

.directive('blackoutHeading', ['$animate', '$timeout', function($animate, $timeout){
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		// scope: {}, // {} = isolate, true = child, false/undefined = no change
		// controller: function($scope, $element, $attrs, $transclude) {},
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		templateUrl: 'partials/heading.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, element, attrs, controller) {

			$scope.$on('changeHeader', function() {
				$timeout(function() {
					element.removeClass('blackout-heading')
					$animate.addClass(element, 'blackout-heading-small').then(function() {
						console.log('animation conplete');
						$scope.$broadcast('loadResultTemplate');
					});
				}, 10);
			});
		}
	};
}])

.directive('templateSwitcher', ['$animate', '$templateRequest', '$templateCache', '$compile', function($animate, $templateRequest, $templateCache, $compile){
	$templateRequest("partials/results.html");
	// Runs during compile
	return {
		// name: '',
		// priority: 1,
		// terminal: true,
		// scope: '=', // {} = isolate, true = child, false/undefined = no change
		// controller: function($scope, $element, $attrs, $transclude) {},
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		// template: '',
		// templateUrl: 'partials/tinput.html',
		// replace: true,
		// transclude: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, element, attrs, controller) {
			
			$scope.removeSearchBar = function() {
				$animate.leave(element.children()[0]);
			};

			$scope.$on('loadResultTemplate', function(){
				var tmpl = $compile($templateCache.get("partials/results.html"))($scope);
				// element.html('');
				element.append(tmpl);
				$scope.$apply();
			});

			// $scope.control.switchTemplate = function() {				
			// 	console.log(resultTemplate);
			// 	var resultTemplate =  $templateCache.get("partials/results.html");
			// 	console.log(resultTemplate);
			// 	var tmp = $compile(resultTemplate)($scope);
			// 	console.log(tmp.toString());
			// 	element.html($compile(resultTemplate)($scope));
			// }
			
			// $scope.switchTemplate = function() {
			// 	console.log("testing");
			// }
		}
	};
}])

.directive('consoleScroll', [function () {
        // Runs during compile
        return {
            // name: '',
            // priority: 1,
            // terminal: true,
            // scope: {}, // {} = isolate, true = child, false/undefined = no change
            // controller: function($scope, $element, $attrs, $transclude) {},
            // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
            restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
            // template: '',
            // templateUrl: '',
            // replace: true,
            // transclude: true,
            // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
            link: function ($scope, element, attrs, controller) {
            	var elm = element[0];
            	$scope.$watch('consoleMsgs.length', function() {
            		elm.scrollTop = elm.scrollHeight;
            	});            	
            }
        };
}]);