/* 
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
*/

'use strict';

// Declare app level module which depends on views, and components
angular.module('blackOut', [
	'ngRoute',
	'ngAnimate',
	'ui.bootstrap.demo',
	'blackOut.config',
	'blackOut.main'
]);

